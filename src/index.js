import React from 'react';
import ReactDom from 'react-dom';
import { HashRouter, Route, Switch } from 'react-router-dom';

// Vistas
import Main from './views/MainPage';
import Cart from './views/CartShopPage';
import Login from './views/LoginPage';

ReactDom.render((
    <HashRouter>
        <Switch>
            <Route path="/login" component={Login} />
            <Route path="/cart" component={Cart} />
            <Route path="/" component={Main} />
        </Switch>
    </HashRouter>
), document.getElementById('root'));