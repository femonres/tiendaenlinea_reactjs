import React, { Component } from 'react';
import { Navbar, Nav, NavItem, Glyphicon, Badge } from 'react-bootstrap';
import { auth } from './Firebase';


export default class TopBar extends Component {

    handleSignOut() {
        auth.doSignOut().then(() => {
            window.location.href='#/login';
        });
    }

    render() {
        return (
            <Navbar fixedTop fluid staticTop>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a href="#">La Bodega</a>
                    </Navbar.Brand>
                </Navbar.Header>
                <Nav pullRight>
                    <NavItem eventKey={1} href="#/">
                        <Glyphicon glyph="th" />
                    </NavItem>
                    <NavItem eventKey={2} href="#/cart">
                        <Glyphicon glyph="shopping-cart" />
                        { (this.props.itemsInCart && this.props.itemsInCart.length > 0) && <Badge>{this.props.itemsInCart.length}</Badge> }
                    </NavItem>
                    <NavItem eventKey={3} >
                        <Glyphicon glyph="inbox" />
                    </NavItem>
                    <NavItem eventKey={4} onSelect={this.handleSignOut}>
                        <Glyphicon glyph="log-out" />
                    </NavItem>
                </Nav>
            </Navbar>
        )
    }
};
