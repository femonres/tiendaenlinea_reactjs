import { db } from './firebase';

export const getProduct = (id) => db.ref('products/' + id);

export const getProducts = () => db.ref('products');

export const getProductsInCart = (user) => db.ref(`products-in-cart/${user}`);