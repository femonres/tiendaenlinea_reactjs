import * as firebase from 'firebase';

const config = {
    apiKey: "AIzaSyCjaOqexPBVLDSXEQ52O_tnpyNPvJsvpuE",
    authDomain: "tienda-online-da3f0.firebaseapp.com",
    databaseURL: "https://tienda-online-da3f0.firebaseio.com",
    projectId: "tienda-online-da3f0",
    storageBucket: "tienda-online-da3f0.appspot.com",
    messagingSenderId: "123593588463"
};

if (! firebase.apps.length) {
    firebase.initializeApp(config);
}

export const auth = firebase.auth();

export const db = firebase.database();