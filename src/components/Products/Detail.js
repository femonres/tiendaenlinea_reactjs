import React, { Component } from 'react';
import { Modal, Button, Image, Row, Col } from 'react-bootstrap';

export default class ProductDetail extends Component {
    render() {
        var picture = `assets/img/products/${this.props.product.picture}`
        return (
            <Modal {...this.props}>
                <Modal.Header closeButton>
                    <Modal.Title>Detalle del producto</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>{this.props.product.name}</h4>
                    <Row>
                        <Col md={6}>
                            <Image src={picture} thumbnail />
                        </Col>
                        <Col md={6}>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus</p>
                            <p>Precio <span>$ {this.props.product.price}</span></p>
                            <p>Unidades disponibles: <span>{this.props.product.avalible}</span></p>
                        </Col>
                    </Row>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.props.onHide}>Cerrar</Button>
                </Modal.Footer>
            </Modal>
        )
    }
};
