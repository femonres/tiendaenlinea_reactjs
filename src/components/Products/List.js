import React, { Component } from 'react';
import { Col } from 'react-bootstrap';

import ProductItem from './Item';

export default class ProductList extends Component {

    render() {
        return this.props.products.map((product, index) => {
            if (product.name.toLowerCase().search(this.props.filter.toLowerCase()) > -1) {
                return (
                    <Col md={3} key={index}>
                        <ProductItem {...this.props} product={product} />
                    </Col>
                )
            }
        });
    }
};
