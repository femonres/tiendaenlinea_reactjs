import React, { Component } from 'react';

import { FormControl, ButtonGroup, Button, Thumbnail, Grid, Row, Col } from 'react-bootstrap';
import ProductDetail from './Detail';


export default class ProductItem extends Component {

    constructor(props) {
      super(props)
    
      this.state = {
         amount: 1,
         detailShow: false
      };
    };
    
    handleFieldChanged(event) {
        this.setState({amount: event.target.value});
    }

    handleAddItemToCart() {
        this.props.add(this.props.product, this.state.amount)
        this.setState({amount: 1});
    }

    render() {
        var image = `assets/img/products/${this.props.product.picture}`;
        let detailClose = () => this.setState({detailShow: false})

        return (
            <Thumbnail src={image} alt="120x80">
                <h3>{this.props.product.name}</h3>
                <p>Precio <span>$ {this.props.product.price}</span></p>
                <p>Unidades disponibles: <span>{this.props.product.avalible}</span></p>
                <Row>
                    <Col md={4}>
                        <Button bsStyle="primary" onClick={() => this.setState({detailShow: true})}>Detalles</Button>
                    </Col>
                    <Col md={4}>
                        <Button bsStyle="warning" onClick={(event) => this.handleAddItemToCart()}>Añadir</Button>
                    </Col>
                    <Col md={4}>
                        <FormControl type="number" min="1" value={this.state.amount} onChange={(event) => this.handleFieldChanged(event)} />
                    </Col>
                </Row>

                <ProductDetail product={this.props.product} show={this.state.detailShow} onHide={detailClose} />
            </Thumbnail>
        )
    }
};
