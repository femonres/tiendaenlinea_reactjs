import React, { Component } from 'react';

import { FormGroup, ControlLabel, FormControl } from 'react-bootstrap';


export default class ProductSearch extends Component {

    render() {
        return (
            <FormGroup controlId="productSearch">
                <ControlLabel>¿Qué estás buscando?</ControlLabel>
                <FormControl placeholder="Buscar producto" type="text" onChange={this.props.search} />
            </FormGroup>
        )
    }
};
