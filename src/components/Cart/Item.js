import React, { Component } from 'react';
import { Image, Row, Media } from 'react-bootstrap';

export default class CartItem extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         picture: '',
         subTotal: 0
      };
    };
    
    componentDidMount() {
        this.setState({picture: `assets/img/products/${this.props.item.product.picture}`});
        this.setState({subTotal: (this.props.item.product.price * this.props.item.amount)});
    }

    render() {
        return (
            <Row className="list-group-item">
                <Media>
                    <Media.Left>
                        <img width={64} height={64} src={this.state.picture} alt="thumbnail" />
                    </Media.Left>
                    <Media.Body>
                        <Media.Heading>{this.props.item.product.name}</Media.Heading>
                        <span><b>Unidades: </b>{this.props.item.amount}</span>
                        <br />
                        <span><b>Subtotal: </b>$ {this.state.subTotal}</span>
                    </Media.Body>
                </Media>
            </Row>
        )
    }
};
