import React, { Component } from 'react';
import { ListGroup } from 'react-bootstrap';
import CartItem from './Item';

export default class CartList extends Component {
    
  render() {
    return (
      <ListGroup>
          {
            this.props.products.map((product, index) => {
                return (<CartItem key={index} item={product} />)
            })
          }
      </ListGroup>
    )
  }
};
