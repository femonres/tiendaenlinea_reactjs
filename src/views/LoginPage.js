import React, { Component } from 'react';
import { Row, Col, Form, FormGroup, ControlLabel, FormControl, Button, Alert } from 'react-bootstrap';

import { auth } from './../components/Firebase';

export default class LoginPage extends Component {

    constructor(props) {
        super(props);
        
        document.title = "Iniciar Sesión | Curso NextU";

        this.state = {
            email: '',
            password: '',
            emailValid: false,
            passwordValid: false,
            formError: null
        };
    }

    handleFieldChanged(event) {
        const name = event.target.name;
        const value = event.target.value;

        this.setState({[name]: value});
    }

    handleSubmitForm(event) {
        event.preventDefault();

        const { history } = this.props;

        auth.doSignInWithEmailAndPassword(this.state.email, this.state.password)
            .then(authUser => {
                console.log("Pasa la validación de usuario", authUser);
                history.push("/");
            })
            .catch(error => {
                console.log("Error al enviar el Login", error);
                this.setState({'formError': error});
            });
    }

    getValidationState(fieldName) {
        switch (fieldName) {
            case 'email':
                if (this.state.email.length == 0) {
                    return null;
                } else if (! this.state.email.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
                    this.state.emailValid = false;
                    return 'error';
                }

                this.state.emailValid = true;
                return 'success';

                break;

            case 'password':
                if (this.state.password.length == 0) {
                    return null;
                } else if (this.state.password.length < 6) {
                    this.state.passwordValid = false;
                    return 'error';
                }

                this.state.passwordValid = true;
                return 'success';

                break;
        
            default:
                break;
        }
    }

    render() {
        return (
            <div className="container-fluid login-layout">
                <Row className="justify-content-center">
                    <Col md={4} className="wrap">
                        <h1 className="form-title">Iniciar Sesión</h1>
                        { this.state.formError && <Alert bsStyle="danger">{this.state.formError.message}</Alert> }
                        <Form horizontal className="login" onSubmit={(event) => this.handleSubmitForm(event)}>
                            <FormGroup controlId="userEmail" validationState={this.getValidationState('email')}>
                                <ControlLabel>Correo electrónico</ControlLabel>
                                <FormControl placeholder="Correo electrónico" type="email" name="email" value={this.state.email} required onChange={(event) => this.handleFieldChanged(event)} />
                                <FormControl.Feedback />
                            </FormGroup>
                            <FormGroup controlId="userPasswd" validationState={this.getValidationState('password')}>
                                <ControlLabel>Contraseña</ControlLabel>
                                <FormControl placeholder="Contraseña" type="password" name="password" value={this.state.password} required onChange={(event) => this.handleFieldChanged(event)} />
                                <FormControl.Feedback />
                            </FormGroup>
                            <Button type="submit" bsStyle="success" disabled={(this.state.emailValid && this.state.passwordValid) == false}>Ingresar</Button>
                        </Form>
                    </Col>
                </Row>
            </div>
        )
    }
};