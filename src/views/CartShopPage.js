import React, { Component } from 'react';
import TopBar from './../components/TopBar';
import { db, firebase } from './../components/Firebase';
import { Panel, Grid, Row, Col, ButtonToolbar, Button } from 'react-bootstrap';
import CartList from './../components/Cart/List';

export default class CartShopPage extends Component {

    constructor(props) {
        super(props)

        document.title = "Tienda onLine | Curso NextU";

        this.state = {
            user: null,
            itemsInCart: [],
            totalValue: 0
        };
    };

    componentDidMount() {
        const { history } = this.props;

        firebase.auth.onAuthStateChanged(authUser => {
            if (authUser) {
                this.setState({user: authUser});
                db.getProductsInCart(authUser.uid).on('value', snapshot => {
                    snapshot.val() ? this.setState({itemsInCart: snapshot.val()}) : null;
                });
            } else {
                history.push("/login");
            }
        });
    }

    shouldComponentUpdate(nextProps, nextState) {
        let totalValue = 0;
        nextState.itemsInCart.map((item, index) => {
            totalValue += (item.product.price * item.amount);
        });

        nextState.totalValue = totalValue;

        return true;
    }

    handlePayItems() {
        const { history } = this.props;

        db.getProductsInCart(this.state.user.uid).remove().then(() => {
            history.push("/");
        })
    }

    handleCancelPay() {
        const { history } = this.props;

        this.state.itemsInCart.map((item, index) => {
            item.product.avalible = (parseInt(item.product.avalible) + parseInt(item.amount))
            db.getProduct(item.id).update(item.product);
        })

        db.getProductsInCart(this.state.user.uid).remove().then(() => {
            history.push("/");
        })
    }

    render() {
        return (
            <div className="container-fluid main-layout">
                <TopBar />

                <Panel>
                    <Panel.Body>
                        <Grid>
                            <Row>
                                <Col md={10}>
                                    <h3>Carrito de compras</h3>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={6}>
                                    <CartList products={this.state.itemsInCart} />
                                </Col>
                                <Col md={6}>
                                    <h1>Total a pagar</h1>
                                    <h2>$ {this.state.totalValue}</h2>

                                    <ButtonToolbar>
                                        <Button bsStyle="warning" disabled={(this.state.itemsInCart && this.state.itemsInCart.length > 0 ) == false} onClick={() => this.handleCancelPay()}>Cancelar</Button>
                                        <Button bsStyle="success" disabled={(this.state.itemsInCart && this.state.itemsInCart.length > 0 ) == false} onClick={() => this.handlePayItems()}>Pagar</Button>
                                    </ButtonToolbar>
                                </Col>
                            </Row>
                        </Grid>
                    </Panel.Body>
                </Panel>
            </div>
        )
    }
};
