import React, { Component } from 'react';
import { Panel, Grid, Row, Col } from 'react-bootstrap';
import TopBar from './../components/TopBar';
import ProductSearch from './../components/Products/Search';
import ProductList from './../components/Products/List';

import { db, firebase } from './../components/Firebase';

export default class MainPage extends Component {

    constructor(props) {
        super(props)

        document.title = "Tienda onLine | Curso NextU";

        this.state = {
            user: null,
            filter: "",
            products: [],
            itemsInCart: []
        };
    };

    componentDidMount() {
        const { history } = this.props;

        firebase.auth.onAuthStateChanged(authUser => {
            if (authUser) {
                this.setState({user: authUser});
                db.getProductsInCart(authUser.uid).on('value', snapshot => {
                    snapshot.val() ? this.setState({itemsInCart: snapshot.val()}) : null;
                });
            } else {
                history.push("/login");
            }
        });

        db.getProducts().on('value', snapshot => {
            this.setState({products: snapshot.val()});
        });
    }

    searchProduct(input) {
        this.setState({filter: input.target.value});
    }

    addToCart(product, amount) {
        var id = this.state.products.indexOf(product);
        product.avalible = (product.avalible - amount);
        db.getProduct(this.state.products.indexOf(product)).update(product);
        db.getProductsInCart(this.state.user.uid).set([...this.state.itemsInCart, {id: id, product: product, amount: amount}]);
    }

    render() {
        return (
            <div>
                <TopBar itemsInCart={this.state.itemsInCart} />
                
                <div className="container-fluid main-layout">
                    <Panel>
                        <Panel.Body>
                            <Grid fluid>
                                <Row>
                                    <Col md={10}>
                                        <h3>Catálogo de Productos</h3>
                                    </Col>
                                    <Col md={2}>
                                        <ProductSearch search={this.searchProduct.bind(this)} />
                                    </Col>
                                </Row>
                                <Row className="scroll-box">
                                    <ProductList products={this.state.products} filter={this.state.filter} add={this.addToCart.bind(this)} />
                                </Row>
                            </Grid>
                        </Panel.Body>
                    </Panel>
                </div>
            </div>
            
        )
    }
};
